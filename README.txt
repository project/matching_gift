Matching Gift Module
http://drupal.org/project/matching_gift


-- OVERVIEW --
This module enables the ability to track the progress of a matching gift
campaign. For example, when Wisconsin Public Radio has an email campaign before
a pledge drive, a certain number of pre-drive pledges are matched by existing
donors and WPR uses this module to communicate how many matching dollars remain.

This module provides the ability to create multiple matching gift campaigns
(entities), a custom block, and configurable messages.


-- INSTALLATION --
Install as usual. For further information, see
https://drupal.org/documentation/install/modules-themes/modules-7.


-- ADMINISTRATION --
The administration page for this module is admin/matching. The module also
provides a Matching Gift Campaigns menu item in your navigation menu that links
to admin/matching.

To create a custom URL for a campaign, add a URL alias at
admin/config/search/path.


-- UPDATE SCRIPTS --
The data used by this module can be updated by hand or you can run a separate
script to update the data in the Drupal database directly. This enables the
module to be used with a non-MySQL database, such as MS SQL, which is not easily
accessible by a Drupal site. To update data directly, use a SQL statement such
as this:

  UPDATE campaign SET amount_raised = 500, updated = 1380077102 WHERE id = 1

For a more complete example, see allegiance_data.php in this module's scripts
directory, but please note that this update script is experimental, so USE AT
YOUR OWN RISK.


-- COMMENTS --
If you find a problem, incorrect comment, obsolete or improper code or such,
please create a new issue at http://drupal.org/project/issues/matching_gift.
