<?php

/**
 * @file
 * This script is a demonstration script. It works with Allegiance fundraising
 * software version 9.1.0. Because Allegiance uses MS SQL, this script only
 * works on a Unix-like machine with a third party library, such as ADOdb:
 * http://phplens.com/lens/adodb/docs-adodb.htm. In this case, the ADOdb
 * directory should be placed in the same directory as this file.
 */

include 'adodb/adodb.inc.php';

// Fill out the information below with the appropriate credentials.
// Determine the current drive.
$drive_id = 1;
$drive_selected = 'PLEDGEDRIVE';
// Source (fundraising) database.
$alleg_dsn = 'MemberData';
$alleg_username = 'AllegUser';
$alleg_password = 'AllegPassword';
// Destination (Drupal) database.
$drupal_username = 'user';
$drupal_password = 'pass';
$drupal_url = '127.0.0.1';
$drupal_db = 'drupal7';

// Connect to the Allegiance datbase.
$alleg_conn = ADONewConnection('mssql') or die("Cannot start ADO");
$alleg_conn->Connect($alleg_dsn, $alleg_username, $alleg_password);
$alleg_conn->SetFetchMode(ADODB_FETCH_ASSOC);

// Get the pledge total from the "verify" data tables.
$total_dollars = 0;
$sql = "SELECT CASE WHEN sum(plamt) IS NULL THEN 0 ELSE CAST(sum(plamt) as decimal) END AS totaldollars
  FROM CampaignTrn WITH (NOLOCK) WHERE cpdcod = '{$drive_selected}'";
$rs = $alleg_conn->Execute($sql);
$count = 0;
while (!$rs->EOF) {
  $total_dollars += $rs->fields('totaldollars');
  $rs->MoveNext();
}

// Get the pledge total from the main database and add it to the "verify"
// totals.
$sql2 = " SELECT CASE WHEN sum(plamt) IS NULL THEN 0 ELSE CAST(sum(plamt) as decimal) END AS totaldollars
  FROM Campaign WITH (NOLOCK) WHERE cmpcod = '{$drive_selected}'";
$rs2 = $alleg_conn->Execute($sql2);
$count = 0;
while (!$rs2->EOF) {
  $total_dollars += $rs2->fields('totaldollars');
  $rs2->MoveNext();
}

// Add the pledge data to the Drupal database.
$current_time = time();
$mysqli = new mysqli($drupal_url, $drupal_username, $drupal_password, $drupal_db);
if (mysqli_connect_errno()) {
  echo "Connection Failed: " . mysqli_connect_errno();
  exit();
}
if ($stmt = $mysqli->prepare("UPDATE campaign SET amount_raised = ?, updated = ? WHERE id = ?")) {
  // Bind parameters: s for string, i for int, etc.
  $stmt->bind_param("iii", $total_dollars, $current_time, $drive_id);
  $stmt->execute();
  $stmt->close();
}
$mysqli->close();
