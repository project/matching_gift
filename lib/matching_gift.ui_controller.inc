<?php

/**
 * @file
 * Contains MatchingGiftUIController.
 */

/**
 * Overrides methods of EntityDefaultUIController.
 */
class MatchingGiftUIController extends EntityDefaultUIController {

  /**
   * Overrides the default table headers in the overview table.
   */
  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $header = $additional_header;
    array_unshift($header, t('Campaigns'));
    if (!empty($this->entityInfo['exportable'])) {
      $header[] = t('Status');
    }
    // Add operations with the right colspan.
    $header[] = array('data' => t('Operations'), 'colspan' => $this->operationCount());
    return $header;
  }
}
