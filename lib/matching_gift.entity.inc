<?php

/**
 * @file
 * Contains MatchingGift.
 */

/**
 * Provides a custom class for overriding methods of the Entity class.
 */
class MatchingGift extends Entity {
  /**
   * Override to allow for a custom default URI.
   */
  protected function defaultUri() {
    return array('path' => 'matching_gift_campaign/' . $this->identifier());
  }
}
