<?php

/**
 * @file
 * Contains MatchingGiftController.
 */

/**
 * Provides a custom matching gift campaign controller.
 */
class MatchingGiftController extends EntityAPIController {

  /**
   * Overrides the buildContent() method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $message = '<div class="matching-gift">';
    $message .= '<div class="matching-gift-title">' . filter_xss_admin($entity->name) . '</div>';
    $message .= '<div class="matching-gift-update">' . t('Data last updated: @date', array('@date' => date("F j, Y, g:i a", $entity->updated))) . '</div>';
    if ($entity->amount_raised < $entity->goal) {
      $message .= '<div class="matching-gift-text">' . filter_xss_admin($entity->in_progress_message) . '</div>';
    }
    if ($entity->amount_raised >= $entity->goal) {
      $message .= '<div class="matching-gift-text">' . filter_xss_admin($entity->success_message) . '</div>';
    }

    $output = array(
      '#type' => 'markup',
      // Make this information appear above and fields the user adds.
      '#weight' => -50,
      '#markup' => format_string($message,
        array(
          '@updated' => date('F j, Y g:ia', $entity->updated),
          '@campaign_goal' => '$' . number_format($entity->goal, 0),
          '@amount_raised' => '$' . number_format($entity->amount_raised, 0),
        )
      ),
    );
    $build['message'] = $output;

    return $build;
  }

  /**
   * Overrides the save() method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (isset($entity->is_new)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->updated = REQUEST_TIME;
    return parent::save($entity, $transaction);
  }
}
