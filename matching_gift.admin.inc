<?php

/**
 * @file
 * Form functions for the Matching Gift module.
 */

/**
 * Provides a configuration form for a matching gift campaign entity.
 */
function matching_gift_campaign_form($form, &$form_state, $campaign = NULL) {
  // Basic campaign information.
  $form['campaign'] = array(
    '#type' => 'fieldset',
    '#title' => t('Campaign information'),
  );
  $form['campaign']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign name'),
    '#description' => t("The name of the matching gift campaign."),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => isset($campaign->name) ? check_plain($campaign->name) : '',
  );
  $form['campaign']['goal'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign Dollar Goal.'),
    '#description' => t("Do not enter any commas, periods, dollar signs, etc."),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => isset($campaign->goal) ? check_plain($campaign->goal) : 0,
  );
  $form['campaign']['amount_raised'] = array(
    '#type' => 'textfield',
    '#title' => t('Current Amount Raised'),
    '#description' => t("Do not enter any commas, periods, dollar signs, etc."),
    '#default_value' => isset($campaign->amount_raised) ? check_plain($campaign->amount_raised) : 0,
    '#size' => 60,
    '#maxlength' => 128,
    '#states' => array(
      'visible' => array(
        ':input[name="data_source"]' => array('value' => 'override'),
      ),
    ),
  );
  $form['campaign']['active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Currently active campaign'),
    '#description' => t('If checked, this campaign will be used for the matching gift campaign block when saved.'),
    '#default_value' => isset($campaign->active) ? $campaign->active : 1,
  );

  // Messages to display.
  $form['messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Messages'),
  );
  $form['messages']['in_progress_message'] = array(
    '#type' => 'textarea',
    '#title' => t('In-Progress Message'),
    '#description' => t('Enter the message to display while a challenge is still in effect. Use @campaign_goal to insert the campaign total and @amount_raised to insert the current total. HTML, such as paragraph tags, can also be used.'),
    '#default_value' => isset($campaign->in_progress_message) ? filter_xss_admin($campaign->in_progress_message) : t("<p>CHALLENGE STILL IN EFFECT! Pledge now to support this drive and your contribution will be DOUBLED by existing members.</p><p>@amount_raised matched</p><p>Current members have issued a challenge and will match pledges made right now, up to @campaign_goal. Don't miss this chance to double your pledge by <a href='http://wpr.org/donate'>pledging now!</a><p>"),
  );
  $form['messages']['success_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Success Message'),
    '#description' => t('Enter the message to display after a successful campaign. Use @campaign_goal to insert the campaign total and @amount_raised to insert the current total. HTML, such as paragraph tags, can also be used.'),
    '#default_value' => isset($campaign->success_message) ? filter_xss_admin($campaign->success_message) : t("<p>Thank you to everyone who helped make this a successful drive. So far we have raised @amount_raised and @campaign_goal was matched by members.</p><p>Your pledge is still important, so <a href='http://wpr.org/donate'>pledge now!</a><p>"),
  );
  $form['messages']['block_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Campaign block message'),
    '#description' => t('Enter the message to display on the matching gift block. Use @campaign_goal, @amount_raised, and @campaign_url to insert dynamic data. HTML, such as paragraph tags, can also be used.'),
    '#default_value' => isset($campaign->block_message) ? filter_xss_admin($campaign->block_message) : '@campaign_goal in pledges will be matched! <a href="@campaign_url">Find out more</a> and <a href="http://wpr.org/donate">pledge</a>!',
  );

  field_attach_form('campaign', $campaign, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($campaign->id) ? t('Update Campaign') : t('Save Campaign'),
    ),
  );
  return $form;
}

/**
 * Processes matching gift campaign settings on submit.
 */
function matching_gift_campaign_form_submit($form, &$form_state) {
  $campaign = entity_ui_form_submit_build_entity($form, $form_state);
  $campaign->save();
  // Store the active campaign ID in the variables table for the block display.
  variable_set('matching_gift_active_campaign', $campaign->id);
  drupal_set_message(t('@name has been saved', array('@name' => $campaign->name)));
  $form_state['redirect'] = 'admin/matching';
}
